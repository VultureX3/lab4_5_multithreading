#pragma once

#include <exception>
#include <string>

class FactorizationException : public std::exception
{
public:
	FactorizationException(const std::string &message);
	~FactorizationException();


	const char* what() const;

private:
	std::string message;
};

class WrongNumber : public FactorizationException
{
public:
	WrongNumber(const std::string &message);
};