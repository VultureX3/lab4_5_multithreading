#pragma once
#include <vector>
#include <string>

using namespace std;

class Factorization {
public:
	Factorization(uint64_t num);

	~Factorization();

	string pprint();

private:
	void factor(uint64_t num, uint64_t start);

	uint64_t num;
	vector<uint64_t> factors;
};