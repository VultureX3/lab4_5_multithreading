#include "FactorizationException.h"

FactorizationException::FactorizationException(const std::string &message) : message(message) { }

FactorizationException::~FactorizationException() { }

const char* FactorizationException::what() const
{
	return message.c_str();
}

WrongNumber::WrongNumber(const std::string &message) : FactorizationException(message) { }