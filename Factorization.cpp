#include <iostream>
#include "Factorization.h"
#include "FactorizationException.h"

using namespace std;

Factorization::Factorization(uint64_t num) : num(num) {
	//uint64_t cur = num, div = 2;
	//while (cur != 1)
	//	if (cur % div != 0)
	//		div++;
	//	else {
	//		cur /= div;
	//		this->factors.push_back(div);
	//	}
	if (num <= 1)
		throw WrongNumber("Wrong Number");
	factor(num, 2);
}

Factorization::~Factorization() {}

string Factorization::pprint() {
	string s;
	// cout << this->num << " = ";
	s = to_string(this->num) + " = ";
	uint64_t prod = 1;
	for (const uint64_t& i : this->factors) {
		prod *= i;
		if (prod == this->num)
			// cout << i << '\n';
			s += to_string(i) + "\n";
		else
			// cout << i << " * ";
			s += to_string(i) + " * ";
	}
	return s;
}

void Factorization::factor(uint64_t num, uint64_t start) {
	if (num == 1)
		return;
	while (num % start != 0)
		start++;
	this->factors.push_back(start);
	factor(num / start, start);
}